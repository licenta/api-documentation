'use strict';

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    config = require('./config.js'),
    extractJsonLike = require('gulp-extract-json-like'),
    jsonLint = require('gulp-json-lint'),
    replace = require('gulp-replace'),
    shell = require('gulp-shell'),
    soften = require('gulp-soften'),
    aglio = require('gulp-aglio'),
    runSequence = require('run-sequence'),
    server = require('gulp-server-livereload'),
    template = require('gulp-template'),
    apiMock = require('api-mock');

var srcDir = './api_blueprint/**/*.apib',
    outDir = './build/',
    outFile = 'output.apib',
    outHtmlFile = 'output.html';

gulp.task('default', ['validate', 'aglio']);

gulp.task('validate', function () {
    return gulp.src(srcDir)
        .pipe(template(config))
        // try to remove useless text
        .pipe(replace(/^( {8}[^{]\S.*|\S.*| {4}(\+|-|\*).*)/gm, ''))
    //.pipe(extractJsonLike())
    //.pipe(jsonLint())
    //.pipe(jsonLint.report('verbose'));
});

gulp.task('serve', function () {
    runSequence('validate', 'aglio', function () {
        gulp.src(outDir)
            .pipe(server({
                livereload: true,
                port: 3000,
                defaultFile: outHtmlFile
            }));

        gulp.watch(srcDir, function () {
            runSequence('validate', 'aglio');
        });
    });
});

gulp.task('api-mock', function () {
    var mockServer = new apiMock({
        blueprintPath: outDir + outFile,
        options: {
            port: 3000
        }
    });
    mockServer.run();
});

gulp.task('aglio', function () {
    gulp.src(srcDir)
        .pipe(template(config))
        .pipe(soften(4))
        .pipe(concat(outFile))
        .pipe(gulp.dest(outDir))
        .pipe(aglio({themeFullWidth: true, themeTemplate: 'triple'}))
        .pipe(gulp.dest(outDir));
});

/*gulp.task('publish', shell.task([
 'export APIARY_API_KEY=' + config.apiary.token + ' && \
 apiary publish --api-name=' + config.apiary.name + ' \
 --path=' + outDir + outFile
 ]));*/