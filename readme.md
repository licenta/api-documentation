## Api documentation for RESTful API

Blueprint API format

### Set up
```
$ npm install
```
- for installing all dependencies

```
$ gulp
```
- for generating static html documentation and full validated .apib document

### Mock server
```
$ gulp api-mock
```
- for starting mock server
