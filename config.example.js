module.exports = {
	"api": {
		"url": "https://licenta-back.dev:8000",
		"api_version": "/api/v1",
		"name" : "Starkiller"
	},
	"apiary": {
		"name": "<%= apiaryName %>",
		"token": "<%= apiaryToken %>"
	},
	"loremipsum": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
}